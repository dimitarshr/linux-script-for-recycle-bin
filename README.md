This pair of Linux shell scripts allow users to ‘remove’ files without them really disappearing. The two scripts are :

* del <path name>
    * This script moves the file located at `<path name>` to the dustbin directory in a manner that allows the file to be 
    restored to its original location later as required. The script is able to cope with absolute path names, relative path names, 
    multiple path names and wildcards.
    * If the dustbin already contains a file with the same name as one being “binned” then it is acceptable that the earlier file is 
    overwritten and lost.
        * E.g.: `del oldfile.txt secret/accounts.xls ~/public_html/old/*.htm*`
    
* restore <file name>
    * This script moves the file called `<file name>` from the dustbin back to its original directory without requiring any 
    further input from the user. The script work independently of the working directory that the user is in when running 
    the command. It is not assumed that the user will be in the same directory when they delete and restore a particular 
    file.
    * If a file of that name already exists at the restore location, the script changes the restored file’s name by 
    appending `.bak` to it to avoid overwriting the existing file. If a deleted file is to be restored into a directory 
    which has since been deleted the restore automatically recreates the directory and any parent directories necessary 
    so the restored file is put back in the same directory that it came from.
        * E.g.: `restore accounts.xlsIf`