#!/bin/sh

# the absolute path for the dustbin
binLocation=/home/dshristov/Documents/Coursework/dustbin/
# In case the dustbin doesn't exist, it is created
# with a .history file inside of it
if !(test -d $binLocation) 
	then
		mkdir $binLocation
fi
touch $binLocation/.history

# If there are 0 arguments, prints helpful text explaining how to use the tool.
if [ $# -eq 0 ]
	then
		echo "del filepath filepath.."
fi

# '$@' means all provided arguments. 
# The quotation marks interpreter each argument literaly and deal with spaces
# In case there is a file name with spaces, it will take the whole name as one argument
for file in "$@"
	do
		filename=$(basename "$file")
		# checks if such file exists
		if test -f "$file"
			then
				mv "$file" $binLocation/"$filename"
				# removes the line from the .history file
				# containing the same file name and writes all the rest back to the .history file
				grep -v -w "$filename" $binLocation/.history > $binLocation/.temp
				mv $binLocation/.temp $binLocation/.history
				# records the absolute path of the deleted file
				readlink -f "$file" >> $binLocation/.history
				echo "Deleting file: $file"				
		else
			echo "There is not a file with the name $filename!"
		fi
	done