#!/bin/sh

# If there are 0 arguments, prints helpful text explaining how to use the tool.
if [ $# -eq 0 ]
	then 
		echo "restore filename.."
else
	filename=$1
	# This variable allows us to restore file from any directory
	# only by specifying the file name
	binLocation=/home/dshristov/Documents/Coursework/dustbin/

	# In case the dustbin doesn't exist, it is created
	# with a .history file inside of it
	if !(test -d $binLocation) 
		then
			mkdir $binLocation
	fi
	touch $binLocation/.history

	# Check if there is a file with the name in the dustbin
	if test -f $binLocation/"$filename"
		then
			# Get the original path of the file from the '.history' file
			path=$(grep -w "$filename" $binLocation/.history)
			# Delete the path of this file from the '.history' file
			grep -v -w "$filename" $binLocation/.history > $binLocation/.outup
			mv $binLocation/.outup $binLocation/.history
			# If the file already exists, append '.bak' at the end.	
			if test -f "$path"
				then
					path="$path".bak
			fi
			# Create the directory if it is deleted.
			if !(test -d $(dirname "$path")) 
				then
					mkdir -p $(dirname "$path")
			fi
			# Move the file.
			mv $binLocation/"$filename" "$path"
			echo "Restoring file $path"
	else
		echo "There is no file in the bin with the name: $filename"
	fi
fi